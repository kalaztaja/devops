## Instructions for testing the system

Pulling the code, going to the root folder, and running docker-compose up -d –build on a machine that has docker-compose installed should be sufficient start. After that the following commands can be used for testing. I recommend using external tools for testing like postman to enable easier put command insertion than doing it manually through browser.
In addition to previous functionality that some might been changed, these were the features that were implemented:

#### 1.	GET /messages

It’ll return all the messages that have been written by the observer. To accomplish this the APIGateway service will ask the previously implemented HTTPService. 

#### 2.	PUT /state

It’ll accept four different parameters in plain text which are INIT, PAUSED, RUNNING and SHUTDOWN.
For Init, it’ll ask Observer to clear out the whole document that had the messages. After that it’ll it will tell Original to start from scratch. 
For PAUSED, it’ll ask the Original to stop sending any messages.
For RUNNING, it’ll ask the original to continue message sending.
And lastly, for SHUTDOWN, it’ll send signals to each container to shut down. It’ll send message through rabbit and then separated HTTP request to HTTP server. After it is all done, it’ll shut down itself. This does not however shut down the rabbit container as the rabbit doesn’t have any external commands and JavaScript has no known way to connect inside the docker container.

#### 3.	Get /state

This will return the state which the Original is currently having. This is done by tracking the originals state by the APIGateway service.

#### 4.	GET /run-log

This will return the history of every PUT /state command excluding the shutdown command. This has been done by utilizing a file that is being updated during every time the actions are called. 
