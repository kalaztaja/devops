var express = require('express');
var fs = require('fs');
var app = express();

app.get('/', function (req, res) {
  fs.readFile('/data/obslogs.txt', 'utf8', (err, data) => {
    if (err) {
      res.send('');
    } else {
      res.attachment('obslogs.txt');
      res.type('txt');
      res.send(data);
    }
  });
});

app.get('/messages', function (req, res) {
  fs.readFile('/data/obslogs.txt', 'utf8', (err, data) => {
    if (err) {
      res.send('');
    } else {
      res.attachment('obslogs.txt');
      res.type('txt');
      const string = JSON.stringify(data);
      res.send(string);
    }
  });
});

app.post('/shutdown', function (req, res) {
  process.exit(0);
});

app.listen(80, function () {
  console.log('express server listening on port 80');
});
