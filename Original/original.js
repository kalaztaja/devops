var amqp = require('amqplib/callback_api');
// STATE 0 EQUALS PAUSED
// STATE 1 EQUALS RUNNING
// STATE 2 EQUALS PANIC
var state = 1;
var iteration = 1;

function sendMessages(channel, exchange) {
  if (state === 1) {
    channel.publish(
      exchange,
      '',
      Buffer.from('MSG_' + iteration.toString())
    );
    iteration++;
  }
  setTimeout(() => {
    sendMessages(channel, exchange);
  }, 3000);
}

amqp.connect('amqp://rabbitmq', function (error0, connection) {
  try {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1;
      }
      var exchange = 'my.o';

      channel.assertExchange(exchange, 'fanout', {
        durable: false,
      });

      var exchange_api = 'my.api';
      state = 1;
      channel.assertExchange(exchange_api, 'fanout', {
        durable: false,
      });
      setTimeout(() => {
        sendMessages(channel, exchange);
      }, 3000);
      channel.assertQueue(
        '',
        {
          exclusive: true,
        },
        function (error2, q) {
          if (error2) {
            throw error2;
          }
          console.log(
            ' [*] Waiting for messages in %s. To exit press CTRL+C',
            q.queue
          );
          channel.bindQueue(q.queue, exchange_api, '');
          console.log('Original listening');
          channel.consume(
            q.queue,
            function (msg) {
              if (msg.content.toString() === 'INIT') {
                try {
                  state = 1;
                  iteration = 1;
                  console.log('Sending');
                } catch (error) {
                  console.log('Error ' + error);
                }
              } else if (msg.content.toString() === 'RUNNING') {
                if (state === 0) {
                  console.log('State is now RUNNING');
                  state = 1;
                }
              } else if (msg.content.toString() === 'PAUSED') {
                if (state === 1) {
                  console.log('STATE IS NOW PAUSED');
                  state = 0;
                }
              } else if (msg.content.toString() === 'SHUTDOWN') {
                process.exit(0);
              }
            },
            {
              noAck: true,
            }
          );
        }
      );
    });
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
});
