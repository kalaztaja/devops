const express = require('express');
const amqp = require('amqplib/callback_api');
const axios = require('axios');
const fs = require('fs');
var moment = require('moment');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.text());

var state = 1;

app.get('/messages', async function (req, res) {
  axios
    .get('http://httpserv:80/messages')
    .then((response) => {
      res.send(response.data);
    })
    .catch((error) => {
      res.send(error);
    });
});

app.get('/state', async function (req, res) {
  res.send(state);
});

app.get('/run-log', async function (req, res) {
  fs.readFile('/data/run-log.txt', 'utf8', (err, data) => {
    if (err) {
      res.send('');
    } else {
      res.send(data);
    }
  });
});

app.put('/state', async function (req, res) {
  const body = req.body;

  if (body === 'INIT' || body === 'PAUSED' || body === 'RUNNING') {
    var CurrentDate = moment().toISOString();
    fs.appendFile(
      '/data/run-log.txt',
      CurrentDate + ' ' + body + '\r\n',
      function (err) {
        if (err) res.send(err);
      }
    );
    amqp.connect('amqp://rabbitmq', function (error0, connection) {
      try {
        if (error0) {
          res.send(error0);
        }
        connection.createChannel(function (error1, channel) {
          if (error1) {
            res.send(error1);
          }
          var exchange = 'my.api';

          channel.assertExchange(exchange, 'fanout', {
            durable: false,
          });
          channel.publish('my.api', '', Buffer.from(body));
        });
        res.send('Job done succesfully');
      } catch (error) {
        console.log(error);
        res.send('Error');
      }
    });
    if (body === 'INIT' || body === 'RUNNING') {
      state = 1;
    } else if (body === 'PAUSED') {
      state = 0;
    }
  } else if (body === 'SHUTDOWN') {
    amqp.connect('amqp://rabbitmq', function (error0, connection) {
      try {
        if (error0) {
          res.send(error0);
        }
        connection.createChannel(function (error1, channel) {
          if (error1) {
            res.send(error1);
          }
          var exchange = 'my.api';

          channel.assertExchange(exchange, 'fanout', {
            durable: false,
          });
          const promise = new Promise((resolve, reject) => {
            channel.publish('my.api', '', Buffer.from(body));
            axios
              .post('http://httpserv:80/shutdown')
              .then(() => {
                resolve();
              })
              .catch((error) => {
                reject(error);
              });
          })
            .then(() => {
              state = 0;
              fs.truncate('/data/run-log.txt', 0, function () {
                console.log('Cleared file');
              });
              setTimeout(() => {
                process.exit(0);
              }, 5000);
            })
            .catch(() => {
              fs.truncate('/data/run-log.txt', 0, function () {
                console.log('Cleared file');
              });
              setTimeout(() => {
                process.exit(0);
              }, 5000);
            });
        });
        res.send('Cleared');
      } catch (error) {
        console.log(error);
        res.send('Error');
      }
    });
  } else {
    res.send('Error');
  }
});

app.listen(8081, function () {
  fs.truncate('/data/run-log.txt', 0, function () {
    console.log('Cleared file');
  });
  console.log('Listening on port 8081');
});
